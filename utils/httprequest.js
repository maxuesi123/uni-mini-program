const brrUrl = 'https://bjwz.bwie.com/mall4j'

class HttpRequest{
	request (
		url,
		method,
		data,
	){
		uni.showLoading({
			title: '正在加载中...',
		})
		return new Promise((resolve, reject) => {
			uni.request({
				url: brrUrl + url,
				data,
				method,
				header:{
					'authorization': uni.getStorageSync('access_token')? "bearer"+uni.getStorageSync("access_token"):""
				},
				success: (res) => {
					
					if (res.statusCode == 200) {
						resolve(res);
						uni.hideLoading()
					} else if (res.statusCode === 401) {
						uni.showToast({
							title: '没有访问权限',
						})
					}
				},
				fail: (err) => {
					uni.hideLoading()
					reject(err);
					uni.showToast({
						title: '请求失败',
					})
				},
				complete() {
					uni.hideLoading()
				}
			})
		})
	}
	get(url,params={}){
		// console.log('get');
		return this.request(url,'GET',params)
	}
	post(url,params){
		// console.log('post');
	 	return this.request(url,'POST',params)
	}
	put(url,params){
		// console.log('post');
	 	return this.request(url,'PUT',params)
	}
	delete(url,params){
		// console.log('post');
	 	return this.request(url,'DELETE',params)
	}
}


export default new HttpRequest()

