const formatUrl = (url, params = {}) => {
    let [pathname, queryString] = url.split('?') //把URL拆成两部分 ？前 ？后 
    if (queryString) {//判断url地址有没有问号
        let urlParams = queryString.replace(/\#\w+$/, '').split('&').map((item) => {//让params跟？后的参数合并
            const [key, val] = item.split('=')
            params[key] = params[key] ? [val, params[key]].toString() : val
            return item
        })
    }
    return pathname + '?' + Object.keys(params).map(key => `${key}=${params[key]}`).join('&')
}
const INDEX_URL = '/pages/index/index '
const navigate = async ({ url, params, redirect = false, ...arg }) => {
    if (!url) {
        throw new Error('跳转页面必须传递url地址')
    }
    let originUrl = url
    url = formatUrl(url, params)
    if (redirect) {
        return await uni.redirectTo({
            url,
            ...arg
        })
    }
    try {
        // console.log(getCurrentPages())
        const pages = getCurrentPages();//页面栈队列
        let delta = -1;
        if (pages.length > 2 && INDEX_URL === url) {
            for (let i = 0; i < pages.length; i++) {
                if (url === pages[i].route) { //入口页面的索引i
                    delta = pages.length - i - 1   // 算出要回退几步回到 入口页面
                    break
                }
            }
        }
        if (delta == -1) {
            return await uni.navigateTo({ //在调用之前判断navigateTo数量
                url, ...arg
            })
        } else {
            return await uni.navigateBack({
                delta: delta
            })
        }

    } catch (error) {
        if (error.errMsg === "navigateTo:fail can not navigateTo a tabbar page") {
            return await uni.switchTab({
                url: originUrl
            })
        }
    }


}
uni.bw_navigate = navigate
export default navigate