import {Login} from '@/api/login.js'
export default{
	namespaced:true,//开启命名空间
	state:()=> ({
		token:uni.getStorageSync('token')||'',
		userInfo:uni.getStorageSync('userInfo')||{}
	}),
	mutations:{
		changeInfo(state,{token,userInfo}){
			// console.log(token,'token');
			state.token = token;
			state.userInfo = userInfo;
			this.commit('user/saveInfo')//更新本地存储 不然首次进来是不正确的
		},
		saveInfo(state){
			// console.log(state.token,'state.token');
			uni.setStorageSync('token_type','bearer')
			uni.setStorageSync('access_token',state.token)
			uni.setStorageSync('userInfo',state.userInfo)
			
		},
		removeStorage(state){//本地清空
			state.token = '';
			state.userInfo = {};
			state.token_type='';
			uni.removeStorageSync('access_token')
			uni.removeStorageSync('userInfo')
			uni.removeStorageSync('token_type')
		}
	},
	actions:{
		async getToken({state,commit},{code,userInfo}){
			// console.log(code,userInfo,'payload')//拿到了code和个人信息
			
			let res = await Login({
				principal:code
			})
			// console.log(res.data,'res111');
			commit('changeInfo',{token:res.data.access_token,userInfo})//触发mutations中的事件 存储到vuex中
		},
		// 路由守卫
		async isLogin({commit,state},payload){
			// console.log('token1111111111111111111',state.token)
			if(state.token){	//判断token有没有
				 return true;
			}
			else{
				// uni.getStorageSync('access_token')
				
				// 没有登陆就跳转到登录页面
				// console.log('跳转到登录页')
				let [error,res] = await uni.showModal({
					title:'登录',
					content:'登录之后才能访问'
				})
				let {confirm,cancel} = res;
				if(confirm){
					// 点的是确定
					uni.navigateTo({
						url:'./../../loginpkg/pages/loginPages/loginPages'
					})
				}
				// console.log(res,'res登录之后才能访问');
				return false;
			}
			
		}
	}
}