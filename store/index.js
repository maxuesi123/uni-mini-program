import Vue from 'vue';
import Vuex from 'vuex';
import total from './modules/total.js';
import user from './modules/user'

Vue.use(Vuex); 
//注册插件  当Vue.use 的时候，会自动去调用插件内部install方法 $router $route

const Store  = new Vuex.Store({
	modules:{
		'toshop':total,
		'user':user,
	}
})

export default Store;