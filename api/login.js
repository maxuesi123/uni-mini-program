import HttpRequest from '../utils/httprequest.js'
export const Login = (data) => HttpRequest.post('/login?grant_type=mini_app', data)