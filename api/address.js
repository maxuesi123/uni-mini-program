import HttpRequest from "@/utils/httprequest.js";

export const Addresscity = (params) =>
  HttpRequest.post("/p/address/addAddr", params);
export const List = () => HttpRequest.get("/p/address/list");
export const Province = (params) =>
  HttpRequest.get("/p/area/listByPid", params);
export const City = (params) => HttpRequest.get("/p/area/listByPid", params);
export const Community = (params) =>
  HttpRequest.get("/p/area/listByPid", params);
export const edityes = (params) =>
  HttpRequest.put("/p/address/updateAddr", params);
export const Del = (params) =>
  HttpRequest.delete(`/p/address/deleteAddr/${params}`);
export const editlist = (params) =>
  HttpRequest.get(`/p/address/addrInfo/${params}`);
