import HttpRequest from "@/utils/httprequest.js";

export const GetCode = (params) => HttpRequest.post("/p/sms/send", params);