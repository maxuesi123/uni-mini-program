import HttpRequest from '../utils/httprequest.js'
// 轮播图接口
export const getIndexImgs = (params) => HttpRequest.get('/indexImgs',params);
// 通知文字轮播接口
export const getTopNoticeList = (params) => HttpRequest.get('/shop/notice/topNoticeList',params);
// 最新公告详情页数据
export const getTopNoticeListInfo = (params) => HttpRequest.get(`/shop/notice/info/${params}`,params);

// 新品推荐
export const getLasted = (params) => HttpRequest.get('/prod/lastedProdPage?current=1&size=10',params);


// // 每日上新标题  : {id: 1, title: "每日上新", seq: "3", style: "2"}
export const getProdTagList = (params) => HttpRequest.get('/prod/tag/prodTagList',params);
// 每日上新 商城热卖 更多宝贝标题 /prod/tag/prodTagList

// 每日上新列表数据
export const getProdListByTagId = (params) => HttpRequest.get('/prod/prodListByTagId?tagId=1&size=6',params);

// 商城热卖列表数据
export const getProdListByTagIdTwo = (params) => HttpRequest.get('/prod/prodListByTagId?tagId=2&size=6',params);
// 更多宝贝列表数据
export const getProdListByTagIdThree = (params) => HttpRequest.get(`/prod/prodListByTagId?tagId=${params.tagId}&size=${params.size}`,params);

// 搜索页面    热门搜索
export const getTopsearch = (params) => HttpRequest.get('/search/hotSearchByShopId?number=10&shopId=1&sort=11',params);


// 搜索结果
export const getSearchProd= (params) => HttpRequest.get(`/search/searchProdPage?current=1&prodName=${params.prodName}&size=10&sort=${params.sort}`,params);

export const prodTagList=(params)=>HttpRequest.post('/p/shopCart/info',params)


