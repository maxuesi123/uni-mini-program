import HttpRequest from '@/utils/httprequest.js'
import httpTool from '@/utils/http.js'
console.log(httpTool)
//购物车页面的数据
const prodTagList=(params)=>HttpRequest.post('/p/shopCart/info',{})



const Addcount= (params)=>HttpRequest.get('/p/shopCart/prodCount',params)
//购物车全选反选
const Shopcarcheck=(params)=>HttpRequest.get('/p/shopCart/totalPay',[])
// 购物车加加  
const shopcarAdd=(params)=>HttpRequest.post('/p/shopCart/changeItem',params)
//购物车删除  
const  ShopcarDelete=(params)=>HttpRequest.delete('/p/shopCart/deleteItem',params);

//全选
const ShopcarTotab=(params)=>HttpRequest.post('/p/shopCart/totalPay',params)

//提交订单 获取商品信息
const ShopcarOrder_Form=(params)=>HttpRequest.post('/p/order/confirm',params)

//提交订单 提交
const ShopSumbit=(params)=>HttpRequest.post('/p/order/submit',params)
// 支付
const ShopPay=(params)=>HttpRequest.post('/p/order/pay',params)

//地址
const ShopcarList=(params)=>HttpRequest.get('/p/address/list',params)
module.exports={
  prodTagList,//购物车数据列表
  Addcount,
  Shopcarcheck,
  ShopcarDelete,
  ShopcarTotab,
  shopcarAdd,//加加
  ShopcarOrder_Form,//提交订单接口
  ShopSumbit,//提交
  ShopPay,//结算
  ShopcarList// 提交订单的地址
}