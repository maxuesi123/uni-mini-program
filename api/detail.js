import HttpRequest from '../utils/httprequest.js';

// 详情数据
export const getprodInfo = (params) => HttpRequest.get(`/prod/prodInfo?prodId=${params.prodId}`,params);
// 评价数据
export const getevaluation = (params) => HttpRequest.get(`/prodComm/prodCommData?prodId=${params.prodId}`,params);



export const geteShopcarList = (params) => HttpRequest.post('/p/shopCart/changeItem',params);

//点击加入购物车
export const gettoShop = ({params}) => HttpRequest.post('/p/shopCart/changeItem',params);
//点击收藏 
export const getCollect = ({params}) => HttpRequest.post('/p/user/collection/addOrCancel',params);
//接收收藏的数据
export const getCollectlist = (params) => HttpRequest.get('/p/user/collection/prods?current=1&size=10',params);
// 点击立即购买
export const getBuy = (params) => HttpRequest.post('/p/order/confirm',params);

