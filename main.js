
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import Store from "./store/index.js";
import Plugins from './plugins/index.js'
Vue.config.productionTip = false

App.mpType = 'app'
Vue.use(Plugins,{
	title:"1909b"
})
// 引入插件
import VueLazyload from 'vue-lazyload'
// 引入navigateTo
import './utils/navigate'

// 注册插件
Vue.use(VueLazyload)

const app = new Vue({
	store:Store,
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif