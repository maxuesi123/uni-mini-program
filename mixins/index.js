import {mapState} from 'vuex';

export default {
	data() {
		return {

		}
	},
	computed: {
		...mapState('toshop', ['total'])
	},
	onShow: function() {
		console.log('App Show');
		this.setTabBarBadge()
	},
	methods: {
		setTabBarBadge() {
			uni.setTabBarBadge({
				index: 2,
				text: this.total + ''
			})
		}
	},
}
